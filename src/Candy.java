import java.util.Comparator;

public class Candy implements Comparable {

    private String name;
    private int cost;
    private int weight;
    private int sugar;
    public Candy(String name, int cost, int weight, int sugar){
    this.name = name;
    this.cost = cost;
    this.weight = weight;
    this.sugar = sugar;
    }

    public String getName() {
return name;
    }

    public int getCost() {
        return cost;
    }

    public int getSugar() {
        return sugar;
    }

    public int getWeight() {
        return weight;
    }



    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public int compareTo(Object o) {
        o = (Candy) o;
        Integer x = ((Candy) o).getCost();
        Integer y = this.cost;
        return x.compareTo(y);
    }
}
