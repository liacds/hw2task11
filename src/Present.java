
import java.util.*;

public class Present implements  Comparator<Candy> {
    ArrayList<Candy> candies = new ArrayList();

    public void addCandy(Candy candy){
        if(!candies.contains(candy)) {
            candies.add(candy);
        }
    }
    public int getPrice(){
        int price =0;
        for(int i =0; i<candies.size(); i++ ){
           price += candies.get(i).getCost();
        }
        return price;
    }
    public int getWeight(){
        int weight =0;
        for(int i =0; i<candies.size(); i++ ){
            weight+= candies.get(i).getCost();
        }
        return weight;
    }
    public void sortByCost(){
        Collections.sort(candies);
    }
    public Candy findCandyInSweetnessDiaposon(int start, int end){
        for(int i =0; i<candies.size(); i++ ) {
            if( candies.get(i).getSugar() >= start && candies.get(i).getSugar() <= end){
                return candies.get(i);
            }
        }
        return null;
        }

    @Override
    public int compare(Candy candy, Candy t1) {
        return 0;
    }
    public String toString(){
        String ret = "";
        for(int i =0; i<candies.size(); i++ ){
            ret += candies.get(i).toString()+" ";
        }
        return ret;
    }
}
